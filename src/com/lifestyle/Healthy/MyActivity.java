package com.lifestyle.Healthy;

import com.smaato.SOMA.SOMABanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

public class MyActivity extends Activity {    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
    	
    	setContentView(R.layout.main);
    	
    	b1 = (Button)findViewById(R.id.button_1);
    	b2 = (Button)findViewById(R.id.button_2);
    	b3 = (Button)findViewById(R.id.button_3);
    	tv1 = (TextView)findViewById(R.id.title_1);
    	tv2 = (TextView)findViewById(R.id.title_2);
    	tv3 = (TextView)findViewById(R.id.title_3);
    	tvo = (TextView)findViewById(R.id.title_out);
    	rb1 = (RadioButton)findViewById(R.id.radio_1);
    	rb2 = (RadioButton)findViewById(R.id.radio_2);
    	
    	loadPrefs();
    	
        b1.setOnClickListener(new Button.OnClickListener() {
          public void onClick(View v) {
        	try {
              short n1 = getNumber( (AutoCompleteTextView)findViewById(R.id.edit_1), g, 50, 250 );
              short n2 = getNumber( (AutoCompleteTextView)findViewById(R.id.edit_2), w, 30, 300 );
              boolean n3 = ((RadioButton)findViewById(R.id.radio_1)).isChecked();
              String s = calc(n1,n2,n3);
              tvo.setText( s.subSequence(0,s.length()) );
            } catch (Exception ex) {
              tvo.setText( error );
            }
          }
        });
        
        b2.setOnClickListener(new Button.OnClickListener() {
          public void onClick(View v) {
        	tvo.setText(info);
          }
        });
        
        b3.setOnClickListener(new Button.OnClickListener() {
          public void onClick(View v) {
          	showDialog( 1 );
          }
        });
        
        SOMABanner mBanner = (SOMABanner)findViewById(R.id.view2);
        mBanner.setPubID("923828498");
        mBanner.setAdID("65732632");
    }
    
    protected Dialog onCreateDialog( int id ) {
		return new AlertDialog.Builder(MyActivity.this)
        .setTitle( R.string._title_lang )
        .setSingleChoiceItems( R.array._entries, my_lang, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            	my_lang = whichButton;
            	boolean lang = (my_lang == 0)?true:false;
            	SharedPreferences preferences = getSharedPreferences( "Healthy", 0 );
            	SharedPreferences.Editor editor = preferences.edit();
            	editor.putBoolean( "language", lang );
            	if( editor.commit() ) setIdLang( lang );
            	dialog.cancel();
            }
        })
       .create();
    }
    
    private void loadPrefs() {
    	SharedPreferences preferences = getSharedPreferences( "Healthy", 0 );
        boolean lang = preferences.getBoolean( "language", false );
        my_lang = (lang)?0:1;
        setIdLang( lang );      
    }
    
    private void setIdLang( boolean l_ru ) {
    	if(l_ru) {
    		b1.setText( R.string.button_1 );
    		b2.setText( R.string.button_2 );
    		b3.setText( R.string.button_3 );
            tv1.setText( R.string.title_1 );
            tv2.setText( R.string.title_2 );
            tv3.setText( R.string.title_3 );
            rb1.setText( R.string.radio_1 );
            rb2.setText( R.string.radio_2 );
            error = R.string.error_text;
            info = R.string.info_text;
            tvo.setText(info);
            h1 = getResources().getStringArray(R.array.h1);
        	h2 = getResources().getStringArray(R.array.h2);
        	h3 = getResources().getStringArray(R.array.h3);
    		g = getString( R.string.growth );
    		w = getString( R.string.weight );
    		nwf = getString( R.string.norm_weight_from );
    		to = getString( R.string.to );
    		kg = getString( R.string.kg );
    		hs = getString( R.string.health_state );
    		ri = getString( R.string.risk );
    		re = getString( R.string.recomend );
    	} else {
    		b1.setText( R.string._button_1 );
    		b2.setText( R.string._button_2 );
    		b3.setText( R.string._button_3 );
            tv1.setText( R.string._title_1 );
            tv2.setText( R.string._title_2 );
            tv3.setText( R.string._title_3 );
            rb1.setText( R.string._radio_1 );
            rb2.setText( R.string._radio_2 );
            error = R.string._error_text;
            info = R.string._info_text;
            tvo.setText(info);
    		h1 = getResources().getStringArray(R.array._h1);
        	h2 = getResources().getStringArray(R.array._h2);
        	h3 = getResources().getStringArray(R.array._h3);
    		g = getString( R.string._growth );
    		w = getString( R.string._weight );
    		nwf = getString( R.string._norm_weight_from );
    		to = getString( R.string._to );
    		kg = getString( R.string._kg );
    		hs = getString( R.string._health_state );
    		ri = getString( R.string._risk );
    		re = getString( R.string._recomend );
    	}
    }
    
    private short getNumber( AutoCompleteTextView t, String type, int minl, int maxl )
      throws Exception {
        String s = t.getText().toString();
        short n = Short.parseShort(s);
        if ( s.length() == 0 || n < minl || maxl < n ) throw new Exception();
        return n;
      }
      
    private String calc(short g, short w, boolean age) throws Exception {
      double iwb = getIwb(g,w);
      double[] nw = getNWeight(g,age);
      String ss = "";
      if( 0 < iwb && iwb < 17.5 ) ss = getMyText(1);
      else if( (age && 17.5 <= iwb && iwb < 19.5) ||
               (!age && 17.5 <= iwb && iwb < 20) ) ss = getMyText(2);
      else if( (age && 19.5 <= iwb && iwb < 23) ||
               (!age && 20 <= iwb && iwb < 26) ) ss = getMyText(3);
      else if( (age && 23 <= iwb && iwb < 27.5) ||
               (!age && 26 <= iwb && iwb < 28) ) ss = getMyText(4);
      else if( (age && 27.5 <= iwb && iwb < 30) ||
               (!age && 28 <= iwb && iwb < 31) ) ss = getMyText(5);
      else if( (age && 30 <= iwb && iwb < 35) ||
               (!age && 31 <= iwb && iwb < 36) ) ss = getMyText(6);
      else if( (age && 35 <= iwb && iwb < 40) ||
               (!age && 36 <= iwb && iwb < 41) ) ss = getMyText(7);
      else if( (age && 40 <= iwb) || (!age && 41 <= iwb) ) ss = getMyText(8);
      else ss = getMyText(0);
      return( ss + " " + nwf + " " + Math.round(nw[0]) + " " + to + " " + Math.round(nw[1]) + " " + kg );
    }
      
    private double getIwb(short g, short w) throws Exception {
      double gm = g / 100.0;
      return( w / (gm * gm) );
    }
      
    private double getW(short g, double myiwb) throws Exception {
      double gm = g / 100.0;
      return( Math.floor(myiwb * gm * gm) );
    }
      
    private double[] getNWeight(short g, boolean a) throws Exception {
      if(a) return( new double[]{getW(g,19.5), getW(g,22.9)} );
      else return( new double[]{getW(g,20), getW(g,25.9)} );
    }
      
    private String getMyText(int n) throws Exception {
      return( hs + " " + h1[n] + " " + ri + " " + h2[n] + " " + re + " " + h3[n] );
    }
    
    public Button b1, b2, b3;
    public TextView tv1, tv2, tv3, tvo;
    public RadioButton rb1, rb2;
    private String g, w, nwf, to, kg, hs, ri, re;
  	private String[] h1, h2, h3;
  	private int my_lang, error, info;
}